'use strict';

angular.module('jollygamesClientApp')
  .controller('EventModalCtrl', function ($scope, $http, socket, $modalInstance) {
    $scope.events = [];


    $scope.addEvent = function() {
      if($scope.newEvent === '') {
        return;
      }
      $http.post('/api/events', { name: $scope.newEvent });
      $scope.newEvent = '';
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });


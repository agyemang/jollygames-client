'use strict';
/* jshint ignore:start */
angular.module('jollygamesClientApp')
  .directive('pageScripts', function($window) {
    return function() {
      var w = angular.element($window);

      /* ======= jQuery Placeholder ======= */
      $('input, textarea').placeholder();

      /* ======= jQuery FitVids - Responsive Video ======= */
      //$(".video-container").fitVids();

      /* ======= Header Background Slideshow - Flexslider ======= */
      /* Ref: https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties */

      $('#bg-slider').flexslider({
        animation: "fade",
        directionNav: false, //remove the default direction-nav - https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
        controlNav: false, //remove the default control-nav
        slideshowSpeed: 6000
      });


      /* ======= FAQ accordion ======= */

      function toggleIcon(e) {
        $(e.target)
          .prev('.panel-heading')
          .find('.panel-title a')
          .toggleClass('active')
          .find("i.fa")
          .toggleClass('fa-plus-square fa-minus-square');
      }
      $('.panel').on('hidden.bs.collapse', toggleIcon);
      $('.panel').on('shown.bs.collapse', toggleIcon);

      /* ======= Fixed header when scrolled ======= */

      w.bind('scroll', function() {
        if (w.scrollTop() > 0) {
          $('#header').addClass('navbar-fixed-top');
        } else {
          $('#header').removeClass('navbar-fixed-top');
        }
      });

      /* ======= Toggle between Signup & Login & ResetPass Modals ======= */
      $('#signup-link').on('click', function(e) {
        $('#signup-modal').modal();
        $('#login-modal').modal('toggle');
        e.preventDefault();
      });

      $('#login-link').on('click', function(e) {
        $('#login-modal').modal();
        $('#signup-modal').modal('toggle');
        e.preventDefault();
      });

      $('#back-to-login-link').on('click', function(e) {
        $('#login-modal').modal();
        $('#resetpass-modal').modal('toggle');
        e.preventDefault();
      });

      $('#resetpass-link').on('click', function(e) {
        $('#login-modal').modal('hide');
        e.preventDefault();
      });

      /* ======= Price Plan CTA buttons trigger signup modal ======= */

      $('#price-plan .btn-cta').on('click', function(e) {
        $('#signup-modal').modal();
        e.preventDefault();
      });


      /* ======= Style Switcher (REMOVE ON YOUR PRODUCTION SITE) ======= */

      $('#config-trigger').on('click', function(e) {
        var $panel = $('#config-panel');
        var panelVisible = $('#config-panel').is(':visible');
        if (panelVisible) {
          $panel.hide();
        } else {
          $panel.show();
        }
        e.preventDefault();
      });

      $('#config-close').on('click', function(e) {
        e.preventDefault();
        $('#config-panel').hide();
      });


      $('#color-options a').on('click', function(e) {
        var $styleSheet = $(this).attr('data-style');
        $('#theme-style').attr('href', $styleSheet);

        var $listItem = $(this).closest('li');
        $listItem.addClass('active');
        $listItem.siblings().removeClass('active');

        e.preventDefault();

      });

      //$('.customer-account .collapse').collapse()



      //Only animate elements when using non-mobile devices
      if (isMobile.any === false) {

        /* Animate elements in #promo (homepage) */
        $('#promo .intro .title').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInLeft delayp1');
          }
        });
        $('#promo .intro .summary').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInRight delayp3');
          }
        });


        /* Animate elements in #why (homepage) */
        /*
         $('#why .benefits').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInLeft delayp1');}
         });

         $('#why .testimonials').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInRight delayp3');}
         });

         $('#why .btn').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInUp delayp6');}
         });
         */


        /* Animate elements in #video (homepage) */
        $('#video .title').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInLeft delayp1');
          }
        });

        $('#video .summary').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInRight delayp3');
          }
        });


        /* Animate elements in #faq */
        /*
         $('#faq .panel').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInUp delayp1');}
         });

         $('#faq .more').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInUp delayp3');}
         });
         */


        /* Animate elements in #features-promo */
        $('#features-promo .title').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInLeft delayp1');
          }
        });

        $('#features-promo .features-list').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInRight delayp3');
          }
        });

        /*
         $('#features-promo .video-container').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInUp delayp6');}
         });

         $('#features .from-left').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInLeft delayp1');}
         });

         $('#features .from-right').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInRight delayp3');}
         });
         */

        /* Animate elements in #price-plan */
        $('#price-plan .price-figure').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInUp delayp1');
          }
        });

        $('#price-plan .heading .label').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInDown delayp6');
          }
        });

        /* Animate elements in #blog-list */
        /*
         $('#blog-list .post').css('opacity', 0).one('inview', function(isInView) {
         if (isInView) {$(this).addClass('animated fadeInUp delayp1');}
         });
         */

        /* Animate elements in #contact-main */
        $('#contact-main .item .icon').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInUp delayp1');
          }
        });

        /* Animate elements in #signup */

        $('#signup .signup-form').css('opacity', 0).one('inview', function(isInView) {
          if (isInView) {
            $(this).addClass('animated fadeInUp delayp1');
          }
        });
      }


    };
  });
/* jshint ignore:end */

'use strict';

angular.module('jollygamesClientApp')
  .controller('MainCtrl', function ($scope, $http, socket,  $modal) {
    $scope.awesomeThings = [];

    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
      socket.syncUpdates('thing', $scope.awesomeThings);
    });

    $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/things', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing');
    });

    $scope.slides = [
      '../assets/images/background/promo-bg.jpg',
      '../assets/images/background/promo-bg-2.jpg',
      '../assets/images/background/promo-bg-3.jpg'
    ];

    $scope.showCreateEventForm = function () {
      var modalInstance = $modal.open({
        templateUrl: '../../components/event/eventModal.html',
        windowClass: 'modal-login modal-event',
        controller: 'EventModalCtrl',
        backdrop: 'static',
        keyboard:false,
        animation: false
      });
    };
  });

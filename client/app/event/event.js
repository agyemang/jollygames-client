'use strict';

angular.module('jollygamesClientApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/event', {
        templateUrl: 'app/event/event.html',
        controller: 'EventCtrl'
      })
      .when('/addevent', {
        templateUrl: 'app/event/create-event.html',
        controller: 'CreateEventCtrl'
      });
  });

'use strict';

angular.module('jollygamesClientApp')
  .controller('CreateEventCtrl', function ($scope, Upload, User) {
    $scope.users = User.query();
    $scope.message = 'Hello';
    $scope.hideUploadButton = false;
    $scope.file = null;

    $scope.$watch('file', function (file) {
      if(file)
        $scope.hideUploadButton = true;
    });
  });

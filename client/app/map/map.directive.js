'use strict';
var map;
angular.module('jollygamesClientApp')
  .directive('map', function () {
    return {
      template: '<div></div>',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        map = new GMaps({
          div: element,
          lat: 51.451573,
          lng: -2.595008,
        });
        map.addMarker({
          lat: 51.451573,
          lng: -2.595008,
          title: 'Address',
          infoWindow: {
            content: '<h5 class="title">Company name</h5><p><span class="region">Address line goes here</span><br><span class="postal-code">Postcode</span><br><span class="country-name">Country</span></p>'
          }

        });
      }
    };
  });

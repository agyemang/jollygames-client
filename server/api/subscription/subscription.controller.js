'use strict';

var _ = require('lodash');
var Subscription = require('./subscription.model');

// Get list of subscriptions
exports.index = function(req, res) {
  Subscription.find(function (err, subscriptions) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(subscriptions);
  });
};

// Get a single subscription
exports.show = function(req, res) {
  Subscription.findById(req.params.id, function (err, subscription) {
    if(err) { return handleError(res, err); }
    if(!subscription) { return res.status(404).send('Not Found'); }
    return res.json(subscription);
  });
};

// Creates a new subscription in the DB.
exports.create = function(req, res) {
  Subscription.create(req.body, function(err, subscription) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(subscription);
  });
};

// Updates an existing subscription in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Subscription.findById(req.params.id, function (err, subscription) {
    if (err) { return handleError(res, err); }
    if(!subscription) { return res.status(404).send('Not Found'); }
    var updated = _.merge(subscription, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(subscription);
    });
  });
};

// Deletes a subscription from the DB.
exports.destroy = function(req, res) {
  Subscription.findById(req.params.id, function (err, subscription) {
    if(err) { return handleError(res, err); }
    if(!subscription) { return res.status(404).send('Not Found'); }
    subscription.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
'use strict';

var mongoose = require('mongoose'),
  NestedSetPlugin = require('mongoose-nested-set'),
  timestamps = require("mongoose-times"),
  uniqueValidator = require('mongoose-unique-validator'),
  Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;

var SubscriptionSchema = new Schema({
  _subscriptionId: ObjectId,
  name: { type: String, required: true },
  info: String,
  start_date: { type: Date},
  end_date: { type: Date},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  active: Boolean,
  plan_id:{type: Schema.Types.ObjectId},
});

// Include plugin
SubscriptionSchema.plugin(NestedSetPlugin);

// Apply the uniqueValidator plugin to userSchema.
SubscriptionSchema.plugin(uniqueValidator);

SubscriptionSchema.plugin(timestamps);

module.exports = mongoose.model('Subscription', SubscriptionSchema);

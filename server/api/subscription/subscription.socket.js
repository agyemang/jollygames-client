/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Subscription = require('./subscription.model');

exports.register = function(socket) {
  Subscription.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Subscription.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('subscription:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('subscription:remove', doc);
}
'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    timestamps = require("mongoose-times"),
    Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
var GroupSchema = new Schema({
  _groupId: ObjectId,
  name: { type: String, index: true, unique: true, required: true },
  info: String,
  price_plans : [{ type: Schema.Types.ObjectId, ref: 'Pricing' }],
  active: Boolean,
});

GroupSchema.plugin(uniqueValidator);
GroupSchema.plugin(timestamps);
module.exports = mongoose.model('Group', GroupSchema);

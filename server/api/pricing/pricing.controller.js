'use strict';

var _ = require('lodash');
var Pricing = require('./pricing.model');

// Get list of pricings
exports.index = function(req, res) {
  Pricing.find(function (err, pricings) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(pricings);
  });
};

// Get a single pricing
exports.show = function(req, res) {
  Pricing.findById(req.params.id, function (err, pricing) {
    if(err) { return handleError(res, err); }
    if(!pricing) { return res.status(404).send('Not Found'); }
    return res.json(pricing);
  });
};

// Creates a new pricing in the DB.
exports.create = function(req, res) {
  Pricing.create(req.body, function(err, pricing) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(pricing);
  });
};

// Updates an existing pricing in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Pricing.findById(req.params.id, function (err, pricing) {
    if (err) { return handleError(res, err); }
    if(!pricing) { return res.status(404).send('Not Found'); }
    var updated = _.merge(pricing, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(pricing);
    });
  });
};

// Deletes a pricing from the DB.
exports.destroy = function(req, res) {
  Pricing.findById(req.params.id, function (err, pricing) {
    if(err) { return handleError(res, err); }
    if(!pricing) { return res.status(404).send('Not Found'); }
    pricing.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
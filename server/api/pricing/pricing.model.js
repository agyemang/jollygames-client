'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  timestamps = require("mongoose-times"),
  uniqueValidator = require('mongoose-unique-validator');
require('mongoose-double')(mongoose);

var SchemaTypes = mongoose.Schema.Types;
var ObjectId = Schema.ObjectId;

var PricingSchema = new Schema({
  _planId: ObjectId,
  name: { type: String, index: true, unique: true, required: true },
  info: String,
  active: Boolean,
  price_per_year: SchemaTypes.Double,
  price_per_month: SchemaTypes.Double,
  price_per_quarter: SchemaTypes.Double,
  price_per_half_year: SchemaTypes.Double,
  price_per_day: SchemaTypes.Double,
  start_date: { type: Date},
  end_date: { type: Date}
});

PricingSchema.plugin(uniqueValidator);
PricingSchema.plugin(timestamps);
module.exports = mongoose.model('Pricing', PricingSchema);

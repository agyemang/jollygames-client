'use strict';

var mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-unique-validator'),
  timestamps = require("mongoose-times"),
  Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
var EventSchema = new Schema({
  _eventId: ObjectId,
  title: { type: String, index: true, required: true },
  description: String,
  active: Boolean,
  img: String,
  start_date : Date,
  end_date : Date,
  start_time: Date,
  end_time: Date,
  recurrance: String,
  occurences:[{ type: String}],
  geoplace: {
    neighborhood: String,
    street_number: String,
    route: String,
    locality: String,
    administrative_area_level_1: String,
    administrative_area_level_2: String,
    administrative_area_level_3: String,
    administrative_area_level_4: String,
    country:String,
    postal_code: String,
    place_id: String,
    formatted_address: String
  },
  organizers: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  price_plans : [{ type: Schema.Types.ObjectId, ref: 'Pricing' }],
  associated_groups : [{ type: Schema.Types.ObjectId, ref: 'Group' }]
});

EventSchema.plugin(timestamps);
EventSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Event', EventSchema);

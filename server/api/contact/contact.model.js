'use strict';

var mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-unique-validator'),
  timestamps = require("mongoose-times"),
    Schema = mongoose.Schema;

var ContactSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});


ContactSchema.plugin(uniqueValidator);
ContactSchema.plugin(timestamps);
module.exports = mongoose.model('Contact', ContactSchema);

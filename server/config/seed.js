/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Event = require('../api/event/event.model');

Thing.find({}).remove(function () {
  Thing.create({
    name: 'Development Tools',
    info: 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.'
  }, {
    name: 'Server and Client integration',
    info: 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.'
  }, {
    name: 'Smart Build System',
    info: 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html'
  }, {
    name: 'Modular Structure',
    info: 'Best practice client and server structures allow for more code reusability and maximum scalability'
  }, {
    name: 'Optimized Build',
    info: 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.'
  }, {
    name: 'Deployment Ready',
    info: 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators'
  });
});

Event.find({}).remove(function () {
  Event.create({
    title: 'Squash Matchups',
    description: 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.',
    img: 'http://fr.bvsport.com/files/2013/01/greg-gaultier-final-bvsport-squash-jp-morgan-toc2.jpg',
    recurrance : 'WEEKLY',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }
  }, {
    title: 'Yoga class',
    description: 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.',
    img: 'http://bbalancedwellnesscenter.com/wp-content/uploads/2015/04/soul-yoga-class-4.jpg',
    recurrance : 'DAILY',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }
  }, {
    title: 'Bar Pool and Poker',
    description: 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html',
    img: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSO3HkW9Q4FhAiGDh4wtgPgJTQVvejG74nZLwWCVps8t1sW3Se5nQ',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }
  }, {
    title: 'Floorball Game',
    description: 'Best practice client and server structures allow for more code reusability and maximum scalability',
    img: 'https://upload.wikimedia.org/wikipedia/commons/c/ca/Floorball_game.jpg',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }

  }, {
    title: 'Table Tennis',
    description: 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.',
    img: 'http://www.aomclub.co.uk/wp-content/uploads/2011/01/table_tennis_01.jpg',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }
  }, {
    title: 'Badminton',
    description: 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators',
    img: 'http://www.japantoday.com/images/size/x420/2013/11/6084b00cc5d7a6346d614315ca190585c93321b2.jpg',
    geoplace: {
      neighborhood: null,
      street_number: null,
      route: null,
      locality: null,
      administrative_area_level_1: null,
      administrative_area_level_2: null,
      administrative_area_level_3: null,
      administrative_area_level_4: null,
      country: null,
      postal_code: null,
      place_id: null,
      formatted_address: null
    }
  });
});

User.find({}).remove(function () {
  User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@test.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@admin.com',
      password: 'admin'
    }, function () {
      console.log('finished populating users');
    }
  );
});
